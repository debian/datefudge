/* vim:ts=2:sts=2:sw=2:et:cindent
 *
 * datefudge64.c: On 32-bit systems provide 64-bit versions of functions
 *                implemented in datefudge.c
 *
 * Copyright (C) 2024, Robert Luberda <robert@debian.org>
 *
 */
#define _GNU_SOURCE

#define _FILE_OFFSET_BITS 64
#define _TIME_BITS 64

#include <features.h>
#include <sys/time.h>
#include <time.h>

#if __TIMESIZE == 32

#pragma message "Compiling 64-bit versions of time functions"

#define time_t __time64_t
#define suseconds_t __suseconds64_t

#define time __time64
#define gettimeofday __gettimeofday64
#define clock_gettime __clock_gettime64

#include "datefudge.c"

#endif
